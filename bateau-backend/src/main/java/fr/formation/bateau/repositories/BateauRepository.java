package fr.formation.bateau.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.bateau.models.Bateau;

@Repository
public interface BateauRepository extends JpaRepository<Bateau, Long> {
    
}
