package fr.formation.bateau.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.bateau.models.Marin;

@Repository
public interface MarinRepository extends JpaRepository<Marin, Long> {
    
}
