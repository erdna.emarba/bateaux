package fr.formation.bateau.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
				.authorizeHttpRequests(customizer -> customizer
						.requestMatchers(HttpMethod.DELETE, "/bateaux/{id}").hasRole("CAPITAINE")
						.requestMatchers("/**").permitAll()
						)
				.formLogin(c -> c.disable())
				.logout(c -> c.disable())
				.csrf(c -> c.disable())
				.httpBasic(c -> {})
				.sessionManagement(c -> c.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
		return http.build();
	}
}
