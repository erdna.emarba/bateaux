package fr.formation.bateau.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.bateau.repositories.BateauRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BateauService {
 
    private BateauRepository bateauRepository;
    
    
    public void deleteById(long id) {
        if (bateauRepository.findById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        bateauRepository.deleteById(id);
    }
}
