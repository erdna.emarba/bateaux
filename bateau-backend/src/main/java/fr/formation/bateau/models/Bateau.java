package fr.formation.bateau.models;

import java.util.Collection;

import org.hibernate.validator.constraints.Length;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Bateau {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    long id;

    @Length(max = 9, min = 9)
    String mmsi;

    @NotBlank
    String nom;

    @OneToMany(mappedBy = "bateau")
    Collection<Marin> marins;

}
