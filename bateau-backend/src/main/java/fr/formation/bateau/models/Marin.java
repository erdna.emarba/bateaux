package fr.formation.bateau.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Marin {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    long id;

    String nom;

    String prenom;

    String matricule;

    @ManyToOne
    Bateau bateau;
    
}
