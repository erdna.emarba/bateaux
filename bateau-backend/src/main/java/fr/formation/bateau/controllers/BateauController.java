package fr.formation.bateau.controllers;

import java.util.Collection;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.bateau.models.Bateau;
import fr.formation.bateau.repositories.BateauRepository;
import fr.formation.bateau.services.BateauService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("bateaux")
public class BateauController {

    private BateauService bateauService;
 
    private BateauRepository bateauRepository;

    @GetMapping()
    public Collection<Bateau> findAll() {
        return bateauRepository.findAll();
    }

    @PostMapping()
    public Bateau save(@Valid @RequestBody Bateau bateau) {
        return bateauRepository.save(bateau);
    }

    @GetMapping("{id}")
    public Bateau findById(@PathVariable long id) {
        return bateauRepository
            .findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable long id) {
        bateauService.deleteById(id);
    }

}
