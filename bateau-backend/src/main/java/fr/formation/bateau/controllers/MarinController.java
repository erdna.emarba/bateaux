package fr.formation.bateau.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.bateau.models.Marin;
import fr.formation.bateau.repositories.MarinRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("marins")
public class MarinController {
 
    private MarinRepository marinRepository;


    @GetMapping("{id}")
    public Marin findById(@PathVariable long id) {
        return marinRepository
            .findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}
