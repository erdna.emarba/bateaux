package fr.formation.bateau.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.bateau.UnitTestBase;
import fr.formation.bateau.models.Bateau;

@SpringBootTest
class BateauServiceTest extends UnitTestBase {
    
    @Autowired
    private BateauService bateauService;

    @Test
    void deleteByIdSuccess() {
        final long ID = 123L;
        // arrange
        var dummyBateau = Bateau.builder().id(ID).build();
        when(bateauRepository.findById(ID)).thenReturn(Optional.of(dummyBateau));
        // act & assert
        assertDoesNotThrow(() -> bateauService.deleteById(ID));
        verify(bateauRepository, times(1)).findById(ID);
        verify(bateauRepository, times(1)).deleteById(ID);
        verifyNoMoreInteractions(bateauRepository);
    }

    @Test
    void deleteByIdFailsIdNotFound() {
        final long ID = 123L;
        // arrange
        when(bateauRepository.findById(anyLong())).thenReturn(Optional.empty());
        // act & assert
        assertThrows(ResponseStatusException.class, () -> bateauService.deleteById(ID));
        verify(bateauRepository, times(1)).findById(ID);
        verifyNoMoreInteractions(bateauRepository);
    }

}
