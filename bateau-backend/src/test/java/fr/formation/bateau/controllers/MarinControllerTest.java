package fr.formation.bateau.controllers;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import fr.formation.bateau.UnitTestBase;
import fr.formation.bateau.models.Marin;


@SpringBootTest
@AutoConfigureMockMvc
public class MarinControllerTest extends UnitTestBase {
    

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void resetMock() {
        reset(marinRepository);
    }

    @Test
    void findByIdSuccess() throws Exception {
        final long ID = 123L;
        // arrange
        var dummyMarin = Marin.builder().id(ID).build();
        when(marinRepository.findById(ID)).thenReturn(Optional.of(dummyMarin));
        // act
        var result = mockMvc.perform(get("/marins/" + ID));
        // assert
        result
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(ID));
        verify(marinRepository, times(1)).findById(ID);
        verifyNoMoreInteractions(marinRepository);
    }

    @Test
    void findByIdErrorIdNotFound() throws Exception {
        // arrange
        when(marinRepository.findById(123L)).thenReturn(Optional.empty());
        // act
        var result = mockMvc.perform(get("/marins/123"));
        // assert
        result
            .andExpect(status().isNotFound());
        verify(marinRepository, times(1)).findById(123L);
        verifyNoMoreInteractions(marinRepository);
    }
}
