package fr.formation.bateau.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import fr.formation.bateau.IntegrationTestBase;

@AutoConfigureMockMvc
public class BateauControllerIT extends IntegrationTestBase {

    @Autowired
    private MockMvc mockMvc;
    
    @Test
    void findByIdSuccess() throws Exception {
        final long ID = 123L;
        // arrange
        // act
        var result = mockMvc.perform(get("/bateaux/" + ID));
        // assert
        result
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(ID));
    }

    @Test
    void findByIdErrorIdNotFound() throws Exception {
        // arrange
        // act
        var result = mockMvc.perform(get("/bateaux/123"));
        // assert
        result.andExpect(status().isNotFound());
    }
    
}
