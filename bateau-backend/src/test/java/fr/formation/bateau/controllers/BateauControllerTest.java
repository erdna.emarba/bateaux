package fr.formation.bateau.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import fr.formation.bateau.UnitTestBase;
import fr.formation.bateau.models.Bateau;
import fr.formation.bateau.services.BateauService;

@SpringBootTest
@AutoConfigureMockMvc
class BateauControllerTest extends UnitTestBase {

    @MockBean
    private BateauService bateauService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void resetMock() {
        reset(bateauRepository);
    }

    @Test
    void saveSuccess() throws Exception {
        // arrange
        var dummyBateau = Bateau.builder().nom("charles de gaulle").mmsi("AZERTYUIO").build();
        when(bateauRepository.save(dummyBateau)).thenReturn(dummyBateau);
        //act
        var result = mockMvc.perform(post("/bateaux")
            .content(objectMapper.writeValueAsString(dummyBateau))
            .contentType(MediaType.APPLICATION_JSON));
        //assert
        result
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.mmsi", is("AZERTYUIO")));
        verify(bateauRepository, times(1)).save(dummyBateau);
        verifyNoMoreInteractions(bateauRepository);
    }

    @Test
    void saveFailedWithAValidationError() throws Exception {
        // arrange
        var dummyBateau = Bateau.builder().nom("charles de gaulle").mmsi("AZE").build();
        when(bateauRepository.save(dummyBateau)).thenReturn(dummyBateau);
        //act
        var result = mockMvc.perform(post("/bateaux")
            .content(objectMapper.writeValueAsString(dummyBateau))
            .contentType(MediaType.APPLICATION_JSON));
        //assert
        result.andExpect(status().isBadRequest());
        verifyNoMoreInteractions(bateauRepository);
    }
    
    @Test
    void findByIdSuccess() throws Exception {
        final long ID = 123L;
        // arrange
        var dummyBateau = Bateau.builder().id(ID).build();
        when(bateauRepository.findById(ID)).thenReturn(Optional.of(dummyBateau));
        // act
        var result = mockMvc.perform(get("/bateaux/" + ID));
        // assert
        result
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(ID));
        verify(bateauRepository, times(1)).findById(ID);
        verifyNoMoreInteractions(bateauRepository);
    }

    @Test
    void findByIdErrorIdNotFound() throws Exception {
        // arrange
        when(bateauRepository.findById(123L)).thenReturn(Optional.empty());
        // act
        var result = mockMvc.perform(get("/bateaux/123"));
        // assert
        result
            .andExpect(status().isNotFound());
        verify(bateauRepository, times(1)).findById(123L);
        verifyNoMoreInteractions(bateauRepository);
    }

    @Test
    @WithMockUser(roles = "CAPITAINE")
    void deleteSuccess() throws Exception {
        // arrange
        // act
        var result = mockMvc.perform(delete("/bateaux/123"));
        // assert
        result.andExpect(status().isOk());
        verify(bateauService, times(1)).deleteById(123L);
        verifyNoMoreInteractions(bateauService);
        verifyNoInteractions(bateauRepository);
    }

    @Test
    @WithMockUser(roles = "CAPITAINE")
    void deleteFailsIdNotFound() throws Exception {
        // arrange
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND)).when(bateauService).deleteById(123L);
        // act
        var result = mockMvc.perform(delete("/bateaux/123"));
        // assert
        result.andExpect(status().isNotFound());
        verify(bateauService, times(1)).deleteById(123L);
        verifyNoMoreInteractions(bateauService);
        verifyNoInteractions(bateauRepository);
    }

    @Test
    void deleteFailsUnauthorized() throws Exception {
        // arrange
        // act
        var result = mockMvc.perform(delete("/bateaux/123"));
        // assert
        result.andExpect(status().isUnauthorized());
        verifyNoInteractions(bateauService);
        verifyNoInteractions(bateauRepository);

    }

    @Test
    @WithMockUser(roles = "MATELOT")
    void deleteFailsForbidden() throws Exception {
        // arrange
        // act
        var result = mockMvc.perform(delete("/bateaux/123"));
        // assert
        result.andExpect(status().isForbidden());
        verifyNoInteractions(bateauService);
        verifyNoInteractions(bateauRepository);

    }
}