package fr.formation.bateau;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import fr.formation.bateau.repositories.BateauRepository;
import fr.formation.bateau.repositories.MarinRepository;

@SpringBootTest
@ActiveProfiles("unit-test")
public class UnitTestBase {

    @MockBean
    protected BateauRepository bateauRepository;
    
    @MockBean
    protected MarinRepository marinRepository;
    
}
