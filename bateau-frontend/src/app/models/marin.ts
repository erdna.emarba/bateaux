import { Bateau } from "./bateau";

export interface Marin {
  prenom: string;
  nom: string;
  bateau: Bateau;
}
