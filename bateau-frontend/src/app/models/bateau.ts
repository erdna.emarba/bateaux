import { Marin } from "./marin";

export interface Bateau {
  id: number;
  mmsi: string;
  nom: string;
  marins: Marin[];
}
