import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BateauService } from './bateau.service';
import { Bateau } from '../models/bateau';
import { Observable, of, throwIfEmpty, timeout } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('BateauService', () => {
  let service: BateauService;
  let httpTestingController: HttpTestingController;

  // beforeEach(() => {
  //   TestBed.configureTestingModule({
  //     imports: [ HttpClientTestingModule ]
  //   });
  //   service = TestBed.inject(BateauService);
  //   httpTestingController = TestBed.inject(HttpTestingController);
  // });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('save: should return the saved boat', () => {
    // configure testbed
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(BateauService);
    httpTestingController = TestBed.inject(HttpTestingController);
    // arrange
    const boat: Bateau = { id: 0, nom: 'charles de gaulle', mmsi: '123456789', marins: [] };
    // act
    const result$ = service.save(boat);
    // assert
    result$
      .pipe(timeout(50))
      .subscribe({
        next: data => {
          expect(data)
            .withContext('the data returned by the the observable should be equal to the one send')
            .toEqual(boat);
        },
        error: () => {
          fail('no error expected here');
        }
      });
    let request = httpTestingController.expectOne("http://vps-3f8dfeab.vps.ovh.net:8080/bateaux");
    request.flush(boat);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(boat);
    httpTestingController.verify();
  });

  it('save: should return the saved boat (spy)', () => {
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('HttpClient', ['post']);
    // configure testbed
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(BateauService);
    // arrange
    const boat: Bateau = { id: 0, nom: 'charles de gaulle', mmsi: '123456789', marins: [] };
    httpClientSpy.post.and.returnValue(of(boat));
    // act
    const result$ = service.save(boat);
    // assert
    result$
      .pipe(timeout(50))
      .subscribe({
        next: data => {
          expect(data)
            .withContext('the data returned by the the observable should be equal to the one send')
            .toEqual(boat);
        },
        error: () => fail('no error expected here')
      });
      expect(httpClientSpy.post.calls.count()).toBe(1);
  });
});
