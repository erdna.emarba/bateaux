import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BasicGroupByOptions, Observable } from 'rxjs';
import { Bateau } from '../models/bateau';

@Injectable({
  providedIn: 'root'
})
export class BateauService {

  backendUrl = "http://vps-3f8dfeab.vps.ovh.net:8080/bateaux"

  constructor(
    private httpClient: HttpClient
  ) { }

  findAll(): Observable<Bateau[]> {
    return this.httpClient.get<Bateau[]>(this.backendUrl);
  }

  findById(id: number): Observable<Bateau> {
    return this.httpClient.get<Bateau>(this.backendUrl + "/" + id);
  }

  save(bateau: Bateau): Observable<Bateau> {
    return this.httpClient.post<Bateau>(this.backendUrl, bateau);
  }

  deleteById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.backendUrl + "/" + id);
  }

}
