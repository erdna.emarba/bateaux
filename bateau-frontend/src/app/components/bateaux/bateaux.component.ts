import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Bateau } from 'src/app/models/bateau';
import { BateauService } from 'src/app/services/bateau.service';

@Component({
  selector: 'app-bateaux',
  templateUrl: './bateaux.component.html',
  styleUrls: ['./bateaux.component.css']
})
export class BateauxComponent implements OnInit {

  bateaux: Bateau[] = [];

  constructor(
    private bateauService: BateauService
  ) {}

  ngOnInit(): void {
    this.bateauService.findAll().subscribe(bateaux => this.bateaux = bateaux);
  }

  onDelete(bateau: Bateau) {
    this.bateauService.deleteById(bateau.id).subscribe(() => {
      const index = this.bateaux.findIndex(b => b.id === bateau.id);
      this.bateaux.splice(index, 1);
    });
  }
}
