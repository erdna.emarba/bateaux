import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BateauxComponent } from './bateaux.component';

describe('BateauxComponent', () => {
  let component: BateauxComponent;
  let fixture: ComponentFixture<BateauxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BateauxComponent]
    });
    fixture = TestBed.createComponent(BateauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
